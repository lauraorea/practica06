from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        # Aqui inicia tu codigo
        if self.cabeza=None:
        self.cabeza= Nodo(valor)
        else:
        actual = self.cabeza
        while actual.siguiente != None:
        actual=actual.siguiente
        actual.siguiente=Nodo(valor)
        return

    def buscar_elemento(self, valor):
        # Aqui inicia tu codigo
        actual = self.cabeza
        while actual != None:
        if actual.valor == valor:
        return True
        else: 
        actual = actual.siguiente
        return False

    def elimina_cabeza(self):
        # Aqui inicia tu codigo
        if self.cabeza != None:
        self.cabeza = self.cabeza.siguiente
        return

    def elimina_rabo(self):
        # Aqui inicia tu codigo
        if self.cabeza == None:
        return
        if self.cabeza.siguiente == None:
        self.cabeza = None
        else: 
        actual = self.cabeza
        while actual.siguiente.siguiente != None:
        actual = actual.siguiente
        actual.siguiente = None
        return

    def tamagno(self):
        # Aqui inicia tu codigo
        contador = 0
        pos = self.cabeza
        while pos != None:
        pos = pos.siguiente
        contador = contador + 1
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
